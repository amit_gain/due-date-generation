import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DailyComponent } from "./frequency/daily/daily.component";
import { WeeklyComponent } from "./frequency/weekly/weekly.component";
import { MonthlyComponent } from "./frequency/monthly/monthly.component";
import { YearlyComponent } from "./frequency/yearly/yearly.component";
import { OneTimeComponent } from "./frequency/one-time/one-time.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SharedResolverService } from './shared/shared-resolver.service';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/daily",
    pathMatch: "full",
  },
  { path: "daily", component: DailyComponent , resolve: {
    data: SharedResolverService
  }},
  { path: "weekly", component: WeeklyComponent },
  { path: "monthly", component: MonthlyComponent },
  { path: "yearly", component: YearlyComponent },
  { path: "one-time", component: OneTimeComponent },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
