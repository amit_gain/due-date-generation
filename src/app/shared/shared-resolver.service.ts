import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { Observable, of } from "rxjs";
import { SharedService } from "./shared.service";

@Injectable({
  providedIn: "root",
})
export class SharedResolverService implements Resolve<any> {
  constructor(private serversService: SharedService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> {
    return of(this.serversService.getTopNavOptions());
  }
}
