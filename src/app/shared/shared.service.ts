import { Injectable } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { log } from "util";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  private routeIndex = new BehaviorSubject("");
  updaterRouteIndex = this.routeIndex.asObservable();
  dueDateObj = {
    daily: [],
    weekly: [],
    monthly: [],
    yearly: [],
    one_time: "",
  };
  private dueDate = new BehaviorSubject(this.dueDateObj);
  updatedueDate = this.dueDate.asObservable();
  topNavOptions = [
    { url: "/daily", name: "Daily", key: "daily" },
    { url: "/weekly", name: "Weekly", key: "weekly" },
    { url: "/monthly", name: "Monthly", key: "monthly" },
    { url: "/yearly", name: "Yearly", key: "yearly" },
    { url: "/one-time", name: "One time", key: "one_time" },
  ];
  constructor() {}

  getTopNavOptions() {
    return this.topNavOptions;
  }
  setRouteIndex(index) {
    this.routeIndex.next(index);
  }
  getRouteIndex() {
    return this.updaterRouteIndex;
  }
  setDueDate(data, type) {
    this.dueDate.subscribe((data) => {
      this.dueDateObj = data;
    });
    if (type === "daily") {
      this.dueDateObj.daily = data;
    } else if (type === "week") {
      this.dueDateObj.weekly = data;
    } else if (type === "monthly") {
      this.dueDateObj.monthly = data;
    } else if (type === "onetime") {
      this.dueDateObj.one_time = data;
    } else if (type === "yearly") {
      this.dueDateObj.yearly = data;
    }
    this.dueDate.next(this.dueDateObj);
  }
  getDueDate(key) {
    console.log(key, this.dueDateObj[key]);

    return this.dueDate;
  }
}
