import { Injectable } from "@angular/core";
import { of } from "rxjs";
import * as moment from "moment";

@Injectable({
  providedIn: "root",
})
export class DailyService {
  constructor() {}
  dailyApi = {
    daily_api: {
      year:2,
      day: "1",
      time: "11:59 AM",
      start_date: moment(new Date()).format("DD-MMM-YYYY"),
      end_date: "",
      weekly_list: [
        { id: "6", name: "Sat" },
        { id: "0", name: "Sun" },
      ],
      weekly_list_details: [
        { id: "1", name: "Mon", isSelected: false },
        { id: "2", name: "Tue", isSelected: true },
        { id: "3", name: "wed", isSelected: false },
        { id: "4", name: "Thu", isSelected: false },
        { id: "5", name: "Fri", isSelected: false },
        { id: "6", name: "Sat", isSelected: false },
        { id: "0", name: "Sun", isSelected: false },
      ],
      monthly_list:[
        { id: '01', name: 'Jan' , isSelected: false},
        { id: '02', name: 'Feb' , isSelected: false},
        { id: '03', name: 'Mar' , isSelected: false},
        { id: '04', name: 'Apr' , isSelected: false},
        { id: '05', name: 'May' , isSelected: false},
        { id: '06', name: 'Jun' , isSelected: false},
        { id: '07', name: 'Jul' , isSelected: false},
        { id: '08', name: 'Aug' , isSelected: false},
        { id: '09', name: 'Sep' , isSelected: false},
        { id: '10', name: 'Oct', isSelected: false },
        { id: '11', name: 'Nov', isSelected: false },
        { id: '12', name: 'Dec', isSelected: false }
      ],
      yearly_list:[
        { id: '01', name: 'Jan' , isSelected: false},
        { id: '02', name: 'Feb' , isSelected: false},
        { id: '03', name: 'Mar' , isSelected: false},
        { id: '04', name: 'Apr' , isSelected: false},
        { id: '05', name: 'May' , isSelected: false},
        { id: '06', name: 'Jun' , isSelected: false},
        { id: '07', name: 'Jul' , isSelected: false},
        { id: '08', name: 'Aug' , isSelected: false},
        { id: '09', name: 'Sep' , isSelected: false},
        { id: '10', name: 'Oct', isSelected: false },
        { id: '11', name: 'Nov', isSelected: false },
        { id: '12', name: 'Dec', isSelected: false }
      ],
      holiday_list: [],
    },
  };
  getDailyApi() {
    return of(this.dailyApi);
  }
}
