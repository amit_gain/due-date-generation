import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DailyComponent } from './frequency/daily/daily.component';
import { WeeklyComponent } from './frequency/weekly/weekly.component';
import { MonthlyComponent } from './frequency/monthly/monthly.component';
import { YearlyComponent } from './frequency/yearly/yearly.component';
import { OneTimeComponent } from './frequency/one-time/one-time.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { NgDatepickerModule } from 'ng2-datepicker';
@NgModule({
  declarations: [
    AppComponent,
    DailyComponent,
    WeeklyComponent,
    MonthlyComponent,
    YearlyComponent,
    OneTimeComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiSwitchModule,
    NgDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
