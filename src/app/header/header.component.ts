import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { SharedService } from "../shared/shared.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  topNavOptions;
  currentIndex;

  constructor(private sharedService: SharedService, private router: Router) {}
  ngOnInit() {
    this.topNavOptions = this.sharedService.getTopNavOptions();
    this.sharedService.getRouteIndex().subscribe((data) => {
      this.currentIndex = data;
    });
  }
  backToOldPage() {
    if (this.currentIndex !== 0) {
      this.router.navigate([this.topNavOptions[this.currentIndex - 1].url]);
    }
  }
}
