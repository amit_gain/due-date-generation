import { Component, OnInit, DoCheck } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { SharedService } from "../shared/shared.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit, DoCheck {
  frequencyName;
  currentIndex;
  topNavOptions;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService
  ) {}
  ngOnInit() {
    this.topNavOptions = this.sharedService.getTopNavOptions();
  }
  ngDoCheck() {
    let self = this;
    this.frequencyName = this.topNavOptions.find((data, index) => {
      if (data.url === self.router.url) {
        self.currentIndex = index;
        return data;
      }
    });
    this.sharedService.setRouteIndex(this.currentIndex);
  }
  nexPage() {
    if (this.currentIndex !== 4) {
      this.router.navigate([this.topNavOptions[this.currentIndex + 1].url]);
    }
    this.sharedService.getDueDate(this.topNavOptions[this.currentIndex].key).subscribe();
  }
}
