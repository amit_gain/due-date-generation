import { Component, OnInit } from "@angular/core";
import { DatepickerOptions } from "ng2-datepicker";
import * as moment from "moment";

import { SharedService } from "src/app/shared/shared.service";

@Component({
  selector: "app-one-time",
  templateUrl: "./one-time.component.html",
  styleUrls: ["./one-time.component.scss"],
})
export class OneTimeComponent implements OnInit {
  bookingStartCalOptions: DatepickerOptions = {
    displayFormat: "DD-MMM-YYYY",
    addClass: "form-control",
    useEmptyBarTitle: false,
  };
  bookingStartopened = false;
  oneTineDate = "";
  constructor(private sharedService: SharedService) {}

  ngOnInit() {}

  calculateDate() {
    this.sharedService.setDueDate(
      moment(this.oneTineDate).format("DD-MMM-YYYY"),
      "onetime"
    );
  }
}
