import { Component, OnInit } from "@angular/core";
import * as moment from "moment";

import { DailyService } from "src/app/shared/daily.service";
import { SharedService } from "src/app/shared/shared.service";
import { DatepickerOptions } from "ng2-datepicker";

@Component({
  selector: "app-weekly",
  templateUrl: "./weekly.component.html",
  styleUrls: ["./weekly.component.scss"],
})
export class WeeklyComponent implements OnInit {
  bookingStartCalOptions: DatepickerOptions = {
    displayFormat: "DD-MMM-YYYY",
    addClass: "form-control",
    useEmptyBarTitle: false,
  };
  bookingEndCalOptions: DatepickerOptions = {
    displayFormat: "DD-MMM-YYYY",
    addClass: "form-control",
    minDate: new Date(),
    useEmptyBarTitle: false,
  };

  bookingStartopened = false;
  weeklyApi;
  dateRange;
  constructor(
    private dailyService: DailyService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.dailyService.getDailyApi().subscribe((data) => {
      this.weeklyApi = data.daily_api;
      this.calculateDate();
    });
  }
  changeDay(index) {
    this.weeklyApi.weekly_list_details[index].isSelected = !this.weeklyApi
      .weekly_list_details[index].isSelected;
    this.calculateDate();
  }
  calculateDate() {
    const dateArray = [];
    this.weeklyApi.start_date = moment(this.weeklyApi.start_date).format("DD-MMM-YYYY");
    let currentDate = moment(this.weeklyApi.start_date);
    let endDate;
    if (this.weeklyApi.end_date != "") {
      this.weeklyApi.end_date = moment(this.weeklyApi.end_date).format("DD-MMM-YYYY");

      endDate = moment(this.weeklyApi.end_date);
    } else {
      endDate = moment(currentDate).add(30, "days");
    }
    while (currentDate <= endDate) {
      dateArray.push(moment(currentDate).format("DD-MMM-YYYY"));
      currentDate = moment(currentDate).add(1, "days");
    }
    this.dateRange = [];
    for (let i = 0; i < dateArray.length; i++) {
      let dataFind = this.weeklyApi.weekly_list_details.filter((data) => {
        if (+data.id === moment(dateArray[i]).day() && data.isSelected) {
          return data;
        }
      });
      if (dataFind.length > 0) {
        this.dateRange.push(dateArray[i]);
      }
    }
    this.sharedService.setDueDate(this.dateRange, "week");
  }
}
