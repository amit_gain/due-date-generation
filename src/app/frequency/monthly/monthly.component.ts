import { Component, OnInit } from "@angular/core";
import * as moment from "moment";

import { DailyService } from "src/app/shared/daily.service";
import { SharedService } from "src/app/shared/shared.service";

@Component({
  selector: "app-monthly",
  templateUrl: "./monthly.component.html",
  styleUrls: ["./monthly.component.scss"],
})
export class MonthlyComponent implements OnInit {
  monthlyData;
  dateRange = [];
  constructor(
    private dailyService: DailyService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.dailyService.getDailyApi().subscribe((data) => {
      this.monthlyData = data.daily_api;
    });
  }
  changeDay(index) {
    let self = this;
    this.dateRange = [];
    this.monthlyData.monthly_list[index].isSelected = !this.monthlyData
      .monthly_list[index].isSelected;
    for (let i = 0; i < 12; i++) {
      if (this.monthlyData.monthly_list[i].isSelected) {
        let tempDate =
          self.monthlyData.monthly_list[i].id +
          "-" +
          self.monthlyData.day +
          "-2020";
        self.dateRange.push(moment(tempDate).format("DD-MMM-YYYY"));
      }
    }
    this.sharedService.setDueDate(this.dateRange, "monthly");
  }
}
