import { Component, OnInit } from "@angular/core";
import * as moment from "moment";

import { DailyService } from "src/app/shared/daily.service";
import { SharedService } from "src/app/shared/shared.service";

@Component({
  selector: "app-yearly",
  templateUrl: "./yearly.component.html",
  styleUrls: ["./yearly.component.scss"],
})
export class YearlyComponent implements OnInit {
  yearlyData;
  monthTemp = "";
  dateRange = [];
  constructor(
    private dailyService: DailyService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.dailyService.getDailyApi().subscribe((data) => {
      this.yearlyData = data.daily_api;
    });
  }

  changeDay(index) {
    let self = this;
    this.dateRange = [];
    this.yearlyData.yearly_list[index].isSelected = true;
    for (let i = 0; i < this.yearlyData.year; i++) {
      let year = 2020 + i;
      let tempDate =
        self.yearlyData.yearly_list[index].id +
        "-" +
        self.yearlyData.day +
        "-" +
        year;
      self.dateRange.push(moment(tempDate).format("DD-MMM-YYYY"));
    }
    this.sharedService.setDueDate(this.dateRange, "yearly");
  }
}
