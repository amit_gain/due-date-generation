import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { DatepickerOptions, NgDatepickerComponent } from "ng2-datepicker";
import { DailyService } from "src/app/shared/daily.service";
import { SharedService } from "src/app/shared/shared.service";
@Component({
  selector: "app-daily",
  templateUrl: "./daily.component.html",
  styleUrls: ["./daily.component.scss"],
})
export class DailyComponent implements OnInit {
  bookingStartCalOptions: DatepickerOptions = {
    displayFormat: "DD-MMM-YYYY",
    addClass: "form-control",
    useEmptyBarTitle: false,
  };
  bookingEndCalOptions: DatepickerOptions = {
    displayFormat: "DD-MMM-YYYY",
    addClass: "form-control",
    minDate: new Date(),
    useEmptyBarTitle: false,
  };
  bookingStartopened = false;
  dailyApi;
  dateRange = [];
  constructor(
    private dailyService: DailyService,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.dailyService.getDailyApi().subscribe((data) => {
      this.dailyApi = data.daily_api;
      this.calculateDate();
    });
  }
  calculateDate() {
    const dateArray = [];
    this.dailyApi.start_date = moment(this.dailyApi.start_date).format("DD-MMM-YYYY");
    let currentDate = moment(this.dailyApi.start_date);
    let endDate;
    if (this.dailyApi.end_date != "") {
      this.dailyApi.end_date = moment(this.dailyApi.end_date).format("DD-MMM-YYYY");

      endDate = moment(this.dailyApi.end_date);
    } else {
      endDate = moment(currentDate).add(30, "days");
    }
    while (currentDate <= endDate) {
      dateArray.push(moment(currentDate).format("DD-MMM-YYYY"));
      currentDate = moment(currentDate).add(1, "days");
    }
    this.dateRange = [];
    for (let i = 0; i < dateArray.length; i++) {
      let dataFind = this.dailyApi.weekly_list.filter((data) => {
        if (+data.id === moment(dateArray[i]).day()) {
          return data;
        }
      });
      if (dataFind.length === 0) {
        this.dateRange.push(dateArray[i]);
      }
    }
    this.sharedService.setDueDate(this.dateRange, "daily");
  }
}
